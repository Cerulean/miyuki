# coding=utf-8

from .handlers import *


def get_routes():
    return [
        ('check', [
            ('([^/]+)', [  # project_id
                ('([^/]+', [  # departure_time_id
                    ('([^/]+)', [  # package_id
                        ('', check.Handler),
                        ('/', check.Handler),
                        ('([^/]+)', check.CheckHandler)
                    ]),
                ]),
            ]),
        ]),
    ]