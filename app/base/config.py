# coding=utf-8

API_VERSION = "1"

# DATABASE

MONGODB_HOST = "localhost"
MONGODB_PORT = 27017
MONGODB_DB = "edoctor"
MONGODB_USER = "shuyu"
MONGODB_PWD = "shuyu"

# REDIS

REDIS_HOST = "localhost"
REDIS_PORT = 6391

# PATTERNS

TEL_PATTERN = "0?(13|14|15|17|18)[0-9]{9}"
PASSWORD_PATTERN = "^[\w]{6,18}$"
NICKNAME_PATTERN = "^[(\u4E00-\u9FA5A-Za-z0-9)|(\u4E00-\u9FA5A)|(\w)]{2,18}$"
TRUENAME_PATTERN = "^[\W]{2,5}$"
YEAR_PATTERN = "201(\d)"
MONTH_PATTERN = "(\d){2}"
AVATAR_TYPE = "(PNG|JPEG|png|jpeg)"

TOKEN_TO_ID_SUFFIX = "_TOKEN_TO_ID"
ID_TO_TOKEN_SUFFIX = "_ID_TO_TOKEN"

SMSCODE_SUFFIX = "_SMSCODE"

# RETURN CODE

CODE_SUCCESS = 0
CODE_FAILURE = -1

# DIR_PATH

ROOT_PATH = ""
IMG_USER_PATH = "static/img/user/"
IMG_AGENCY_PATH = "static/img/agency/"
IMG_BANNER_PATH = "static/img/banner/"
IMG_PROJECT_PATH = "static/img/project/"
IMG_TOPIC_PATH = "static/img/topic/"

UEDITOR_PATH = "static/ueditor/python/"

# HOST_NAME

DEBUG_HOSTNAME = 'http://api.in.actopper.com'

# SSO TYPE

SSO_TYPE_WECHAT = 0
SSO_TYPE_WEIBO = 1

# Wechat

WECHAT_APP_ID = "wx81bc33b2886ddff2"
WECHAT_APP_SECRET = "dcf2cff4cfe306954f43b9f0b8f449b1"

# Weibo

WEIBO_APP_ID = "3659883749"
WEIBO_APP_SECRET = "5b1988e57f25aad07bcd13ffe8b5d7a7"


