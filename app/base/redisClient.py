# coding=utf-8

from app.base import config
from app.base.helpers.tools import *

import redis


def create_redis_client():
    return redis.Redis(decode_responses=True)


class RedisClient(object):
    redis_client = create_redis_client()

    @classmethod
    def get_smscode(cls, username):
        return cls.redis_client.get(username + config.SMSCODE_SUFFIX)

    @classmethod
    def set_smscode(cls, useranme, smscode):
        cls.redis_client.set(useranme + config.SMSCODE_SUFFIX, smscode, ex=60 * 5)

    @classmethod
    def get_user_id(cls, token):
        return cls.redis_client.get(token + config.TOKEN_TO_ID_SUFFIX)

    @classmethod
    def get_token(cls, user_id):
        return cls.redis_client.get(user_id + config.ID_TO_TOKEN_SUFFIX)

    @classmethod
    def generate_token_if_need(cls, user_id):
        token = cls.get_token(user_id)
        if not token:
            token = Tools.md5(user_id + str(time.time()))
            cls.redis_client.set(user_id + config.ID_TO_TOKEN_SUFFIX, token, ex=60 * 60 * 24 * 7)
            cls.redis_client.set(token + config.TOKEN_TO_ID_SUFFIX, user_id, ex=60 * 60 * 24 * 7)
        return token
