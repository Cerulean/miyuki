# coding=utf-8

from tornado.web import RequestHandler, HTTPError, MissingArgumentError

import tornado.web
import json

from mongoengine import QuerySet

from app.base import config
from app.base.redisClient import *
from app.base.models.account import *
from app.base.helpers.serializer import json_encode

logged = tornado.web.authenticated


class BaseRequestHandler(RequestHandler):

    def render_json(self, code, msg=None, results=None, method=None):

        if method:
            if isinstance(results, list) or isinstance(results, QuerySet):

                _results = list()

                for obj in results:
                    if hasattr(obj, method):
                        _results.append(getattr(obj, method)())

            else:

                _results = dict()

                if hasattr(results, method):
                    _results = getattr(results, method)()

        else:

            _results = results if results else {}

        self.set_header("Content-Type", "application/json")
        return self.write(json_encode({
            "code": code,
            "msg": msg,
            "results": _results,
        }))

    @staticmethod
    def raise_http_error(code, log_message=None):
        raise HTTPError(code, log_message)


class WebRequestHandler(BaseRequestHandler):

    def get_argument(self, name, required=True, default=None):
        try:
            return super(BaseRequestHandler, self).get_argument(name)
        except MissingArgumentError:
            if required:
                raise HTTPError(400)
            else:
                return default

    def get_file_argument(self, name, required=False):
        files = self.get_files_argument(name, required)
        if files is None:
            if required:
                self.raise_http_error(400)
            else:
                return None
        else:
            return files[0]

    def get_files_argument(self, name, required=False):
        try:
            files = self.request.files[name]
            if len(files) > 0:
                return files
        except KeyError:
            pass

        if required:
            self.raise_http_error(400)
        else:
            return None

    def save_file(self, file, savepath, filename):
        ext = file.filename.split('.')[-1]
        import os
        full_savepath = os.path.join(config.ROOT_PATH, savepath)
        if not os.path.exists(full_savepath):
            os.makedirs(full_savepath)
        full_filename = os.path.join(full_savepath, filename + "." + ext)
        with open(full_filename, 'wb') as fn:
            fn.write(file['body'])

        return os.path.join("/", savepath, filename + "." + ext)


class ApiBaseRequestHandler(BaseRequestHandler):

    def __init__(self, application, request, **kwargs):
        super(BaseRequestHandler, self).__init__(application, request, **kwargs)
        self.redis_cli = RedisClient

    def get_current_user(self):
        token = self.get_argument('token', None)
        if not token:
            raise HTTPError(401)
        user_id = self.redis_cli.get_user_id(token)
        if not user_id:
            raise HTTPError(401)
        return User.get_object_by_id(user_id)


class FormRequestHandler(ApiBaseRequestHandler):
    def __init__(self, application, request, **kwargs):
        super(FormRequestHandler, self).__init__(application, request, **kwargs)

    def get_files_argument(self, name):
        try:
            files = self.request.files[name]
            if len(files) > 0:
                return files
            else:
                raise HTTPError(400)
        except KeyError:
            raise HTTPError(400)


class JsonRequestHandler(ApiBaseRequestHandler):
    def __init__(self, application, request, **kwargs):
        super(JsonRequestHandler, self).__init__(application, request, **kwargs)
        self.json = None

    _ARG_DEFAULT = []

    def get_argument(self, name, default=_ARG_DEFAULT, strip=True):
        if self.request.method == "GET" or self.request.method == 'DELETE':
            return super(JsonRequestHandler, self).get_query_argument(name, default, strip)
        else:
            if self.json is None:
                self.json = json.loads(self.request.body.decode('utf-8'))
            try:
                return self.json[name]
            except KeyError or ValueError:
                if (isinstance(default, list) and len(default)) or not isinstance(default, list):
                    return default
                else:
                    raise HTTPError(400)
