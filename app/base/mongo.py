# coding=utf-8

from app.base import config
from mongoengine import connect


def connect_mongo():
    connect(
        db=config.MONGODB_DB,
        host=config.MONGODB_HOST,
        port=config.MONGODB_PORT,
        username=config.MONGODB_USER,
        password=config.MONGODB_PWD
    )



