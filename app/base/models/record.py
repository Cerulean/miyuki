# coding=utf-8

from app.base.models.baseDocument import BaseDocument
from mongoengine.fields import *


class Record(BaseDocument):

    doctor_id = ObjectIdField()

    patient_id = ObjectIdField()

    complaint = StringField()

    inspect = StringField()

    diagnosis = StringField()

    treat = StringField()

    create_date = DateTimeField()

    def to_record_info(self):
        record_dic = self.to_mongo()

        record_dic.pop('doctor_id')
        record_dic.pop('patient_id')

        return record_dic
