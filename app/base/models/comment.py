# coding=utf-8

from app.base.models.baseDocument import BaseDocument
from app.base.models.account import User

from mongoengine.fields import *


class Comment(BaseDocument):

    doctor_id = ObjectIdField()

    user_id = ObjectIdField()

    content = StringField()

    create_date = DateTimeField()

    grade = IntField()

    def to_comment_info(self):

        user = User.get_object_by_id(self.user_id)

        comment_dic = self.to_mongo()
        comment_dic.pop('doctor_id')
        comment_dic.pop('user_id')
        comment_dic['comment_by'] = user.truename

        return comment_dic
