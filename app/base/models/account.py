# coding=utf-8

from app.base import config
from app.base.models.baseDocument import BaseDocument
from app.base.models.location import City

from mongoengine.fields import *
from mongoengine import EmbeddedDocument


class Hospital(EmbeddedDocument):

    name = StringField()

    # 0, 三级甲等
    # 1, 三级乙等

    level = IntField()

    address = StringField()

    city_id = ObjectIdField()


class Department(EmbeddedDocument):

    name = StringField()


class User(BaseDocument):
    username = StringField(required=True)

    password = StringField(required=True)

    nickname = StringField(default="骨管家用户")

    truename = StringField()

    # 0, 用户
    # 1, 医生

    identiy = IntField(default=0)

    speciality = StringField()

    hospital = EmbeddedDocumentField(Hospital)

    department = EmbeddedDocumentField(Department)

    city_id = ObjectIdField(default=City.get_objects(name='西安市').first().id)

    grade = FloatField()

    # 0, 主任医师
    # 1, 副主任医师
    # 2, 主治医师
    # 3, 副主治医生
    # 4, 住院医师

    level = IntField(default=4)

    email = EmailField()

    intro = StringField()

    patients = ListField()

    doctors = ListField()

    avatar_url = StringField()

    wechat_id = StringField()

    weibo_id = StringField()

    @staticmethod
    def get_user_by_username(username):
        return User.objects(username=username).first() if username else None

    @staticmethod
    def has_registered(username):
        return True if User.objects(username=username).first() else False

    def to_user_info(self):

        user_dic = self.to_mongo()
        user_dic.pop('password')
        user_dic.pop('patients')
        user_dic.pop('level')

        user_dic.pop('city_id')

        city = City.get_object_by_id(self.city_id)

        user_dic['city'] = city

        if self.avatar_url:

            avatar_url = config.DEBUG_HOSTNAME + self.avatar_url

            user_dic['avatar_url'] = avatar_url

        return user_dic

    def to_doctor_info(self):

        doctor_info = self.to_mongo()
        doctor_info.pop('password')
        doctor_info.pop('city_id')
        doctor_info.pop('patients')

        city = City.get_object_by_id(self.city_id)

        doctor_info['city'] = city

        city = City.get_object_by_id(self.hospital.city_id)

        doctor_info['hospital'].pop('city_id')
        doctor_info['hospital']['city'] = city

        doctor_info['patients_count'] = len(self.patients)

        if self.avatar_url:

            avatar_url = config.DEBUG_HOSTNAME + self.avatar_url

            doctor_info['avatar_url'] = avatar_url

        return doctor_info
