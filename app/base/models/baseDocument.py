# coding=utf-8

from mongoengine import Document


class BaseDocument(Document):

    meta = {
        'abstract': True
    }

    @classmethod
    def get_objects(cls, last_id=None, order_by='-id', *q_obj, **kwargs):

        parms = kwargs if kwargs else dict()

        if last_id:
            parms['id__lt'] = last_id

        objects = cls.objects(*q_obj, **parms).order_by(order_by).limit(15)

        return objects

    @classmethod
    def get_object_by_id(cls, object_id):
        return cls.objects(id=object_id).first() if object_id else None
