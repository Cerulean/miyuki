# coding=utf-8

from app.base.models.baseDocument import BaseDocument

from mongoengine.fields import *


class City(BaseDocument):
    name = StringField()


class Province(BaseDocument):
    name = StringField()
