# coding=utf-8

from mongoengine.fields import ObjectId, Document, QuerySet
from datetime import datetime
from app.base.models.baseDocument import BaseDocument

import json


def customized_json_dumps(obj):
    """

    这里只需判断原生 JSONEncode 无法识别的类型
    :param obj:

    """
    if isinstance(obj, ObjectId):
        return str(obj)
    elif isinstance(obj, datetime):
        return obj.strftime('%Y-%m-%d %H:%M:%S')
    elif isinstance(obj, BaseDocument):
        return obj.to_mongo()
    elif isinstance(obj, QuerySet):
        return [sub for sub in obj]
    else:
        return str(obj)


def json_encode(obj):
    return json.dumps(obj, default=customized_json_dumps)
