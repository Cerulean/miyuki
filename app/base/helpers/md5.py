# coding=utf-8

import hashlib


def md5(raw_str):
    m = hashlib.md5()
    m.update(raw_str.encode('utf-8'))
    return m.hexdigest().upper()
