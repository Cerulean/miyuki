# coding=utf-8

import re
import time
import hashlib


class Tools(object):

    @classmethod
    def md5(cls, raw_str):
        m = hashlib.md5()
        m.update(raw_str.encode('utf-8'))
        return m.hexdigest().upper()
