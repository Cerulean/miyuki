# coding=utf-8


def print_obj(obj):
    print(', '.join(['%s:%s' % item for item in obj.__dict__.items()]))