# coding=utf-8

from app.api.handlers import *


def get_routes():
    return [
        ('account', [
            ('smscode', account.SmscodeHandler),
            ('register', account.RegisterHandler),
            ('login', account.LoginHandler),
            ('logout', account.LogoutHandler),
            ('forget', account.ForgetHandler),
            ('info', account.InfoHandler),
            ('avatar', account.AvatarHandler),
            ('password', account.PasswordHandler),
            ('sso', account.SsoHandler),
            ('bind', account.BindHandler)
        ]),
        ('patient', [
            ('record', patient.RecordHandler),
            ('doctor', patient.DoctorHandler),
        ]),
        ('doctor', [
            ('search', doctor.SearchHandler),
            ('comment', doctor.CommentHandler),
            ('record', doctor.RecordHandler),
            ('patient', doctor.PatientHandler),
            ('list', doctor.DoctorHandler)
        ]),
        ('location', [
            ('cities', location.CitiesHandler)
        ]),
        ('test', [
            ('doctor', test.DoctorHandler),
            ('patient', test.PatientHandler),
            ('comment', test.CommentHandler),
            ('record', test.RecordHandler)
        ])
    ]
