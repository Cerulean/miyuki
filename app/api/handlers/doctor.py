# coding=utf-8

from app.base.requestHandler import JsonRequestHandler, logged
from app.base.models.account import User
from app.base.models.comment import Comment
from app.base.models.record import Record
from app.base.config import *

from mongoengine.fields import ObjectId

from datetime import datetime

class SearchHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):

        search_type = self.get_argument('search_type')

        last_id = self.get_argument('last_id', None)

        # 0, 按医院地区
        # 1, 按医院等级
        # 2, 按医生评分

        if search_type == '0':

            city_id = self.get_argument('city_id')

            doctors = User.get_objects(last_id, 'hospital__level', identiy=1, hospital__city_id=ObjectId(city_id))

        elif search_type == '1':

            level = self.get_argument('level')

            doctors = User.get_objects(last_id, '-level', identiy=1, hospital__level=level)

        elif search_type == '2':

            doctors = User.get_objects(last_id, '-grade', identiy=1)

        else:

            return self.render_json(CODE_FAILURE, '搜索类型错误')

        return self.render_json(CODE_SUCCESS, '获取医生列表成功', doctors, 'to_doctor_info')


class RecordHandler(JsonRequestHandler):
    @logged
    def get(self, *args, **kwargs):

        user = self.current_user

        patient_id = self.get_argument('patient_id')

        last_id = self.get_argument('last_id', None)

        records = Record.get_objects(last_id, '-id', patient_id=patient_id, doctor_id=user.id)

        self.render_json(CODE_SUCCESS, '获取病例成功', records, 'to_record_info')

    @logged
    def post(self, *args, **kwargs):

        user = self.current_user

        patient_id = self.get_argument('patient_id')

        complaint = self.get_argument('complaint')

        inspect = self.get_argument('inspect')

        diagnosis = self.get_argument('diagnosis')

        treat = self.get_argument('treat')

        create_date = datetime.today()

        record = Record()
        record.patient_id = patient_id
        record.doctor_id = user.id
        record.complaint = complaint
        record.inspect = inspect
        record.diagnosis = diagnosis
        record.treat = treat
        record.create_date = create_date
        record.save()

        self.render_json(CODE_SUCCESS, '添加病历成功')


class DoctorHandler(JsonRequestHandler):
    @logged
    def get(self, *args, **kwargs):

        user = self.current_user

        doctors = user.doctors

        self.render_json(CODE_SUCCESS, '获取医生成功', doctors, 'to_doctor_info')

    @logged
    def post(self, *args, **kwargs):

        doctor_id = self.get_argument('doctor_id')

        user = self.current_user

        doctor = user.get_object_by_id(doctor_id)

        if doctor in user.doctors:
            return self.render_json(CODE_FAILURE, '医生已经存在')

        user.doctors.append(doctor)
        user.save()

        return self.render_json(CODE_SUCCESS, '医生添加成功')

    @logged
    def delete(self, *args, **kwargs):

        doctor_id = self.get_argument('doctor_id')

        user = self.current_user

        doctor = user.get_object_by_id(doctor_id)

        if doctor in user.doctors:
            user.doctors.remove(doctor)
            user.save()

        return self.render_json(CODE_SUCCESS, '医生删除成功')


class CommentHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):

        doctor_id = self.get_argument('doctor_id')

        last_id = self.get_argument('last_id', None)

        comments = Comment.get_objects(last_id, '-id', doctor_id=doctor_id)

        self.render_json(CODE_SUCCESS, '获取评论成功', comments, 'to_comment_info')


class PatientHandler(JsonRequestHandler):
    @logged
    def get(self, *args, **kwargs):

        user = self.current_user

        patients = user.patients

        self.render_json(CODE_SUCCESS, '获取病人成功', patients, 'to_user_info')