# coding=utf-8

from app.base.requestHandler import JsonRequestHandler, logged
from app.base.models.account import User
from app.base.models.comment import Comment
from app.base.models.record import Record
from app.base.config import *

from mongoengine.fields import ObjectId


class DoctorHandler(JsonRequestHandler):
    @logged
    def get(self, *args, **kwargs):

        user = self.current_user

        last_id = self.get_argument('last_id', None)

        doctors = User.get_objects(last_id, '-id', identiy=1, patients=user)

        self.render_json(CODE_SUCCESS, '获取医生成功', doctors, 'to_doctor_info')

    @logged
    def post(self, *args, **kwargs):

        user = self.current_user

        doctor_id = self.get_argument('doctor_id')

        doctor = User.get_object_by_id(doctor_id)

        if user not in doctor.patients:
            doctor.patients.append(user)

        return self.render_json(CODE_SUCCESS, '预约医生成功')


class RecordHandler(JsonRequestHandler):
    @logged
    def get(self, *args, **kwargs):

        user = self.current_user

        last_id = self.get_argument('last_id', None)

        records = Record.get_objects(last_id, '-id', patient_id=user.id)

        self.render_json(CODE_SUCCESS, '获取病例成功', records, 'to_record_info')