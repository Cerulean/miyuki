# coding=utf-8

from app.base.requestHandler import JsonRequestHandler
from app.base.models.account import User, Hospital, Department
from app.base.models.comment import Comment
from app.base.models.record import Record

from mongoengine.fields import ObjectId

from random import randint, uniform, choice
from datetime import datetime


class DoctorHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):
        for index in xrange(10, 50):
            username = '186293573' + str(index)

            password = 'E10ADC3949BA59ABBE56E057F20F883E'

            truenames = ['万丽娜', '李艺彤', '冯薪朵', '鞠婧祎', '李佳恩', '杨韫玉', '邵雪聪', '许杨玉琢']

            truename = choice(truenames)

            nicknames = ['小万', '小李', '小冯', '小鞠', '小李', '小杨', '小邵', '小许']

            nickname = choice(nicknames)

            identiy = 1

            specialitys = ['骨质疏松', '跌打扭伤', '断肢再植', '创伤骨折']

            speciality = choice(specialitys)

            hospitals = [Hospital(name='西京医院',
                                  level=0,
                                  address='新城区长乐西路127号',
                                  city_id=ObjectId("570e58180e7051acb7ad32df")),
                         Hospital(name='陕西省人民医院',
                                  level=0,
                                  address='碑林区黄雁村友谊西路256号',
                                  city_id=ObjectId("570e58180e7051acb7ad32df")),
                         Hospital(name='交通大学第一附属医院',
                                  level=0,
                                  address='陕西省西安市雁塔区雁塔西路277',
                                  city_id=ObjectId("570e58180e7051acb7ad32df")),
                         Hospital(name='陕西省红会医院',
                                  level=0,
                                  address='友谊东路555号',
                                  city_id=ObjectId("570e58180e7051acb7ad32df")),
                         Hospital(name='咸阳市第一人民医院',
                                  level=1,
                                  address='毕塬西路',
                                  city_id=ObjectId("570e581d0e7051acb7ad32e0"))]

            hospital = choice(hospitals)

            departments = ['创伤骨科', '普通骨科']

            department = Department(name=choice(departments))

            city_id = ObjectId("570e58180e7051acb7ad32df")

            grade = uniform(0, 5)

            level = randint(0, 4)

            intro = choice(['专治各种不服', '我擅长起死回生', '医者仁心', '救死扶伤'])

            patiens = User.objects(identiy=0)

            user = User()
            user.username = username
            user.password = password
            user.nickname = nickname
            user.truename = truename
            user.identiy = identiy
            user.speciality = speciality
            user.hospital = hospital
            user.department = department
            user.city_id = city_id
            user.grade = grade
            user.level = level
            user.intro = intro
            user.patients = [choice(patiens), choice(patiens), choice(patiens), choice(patiens)]
            user.save()


class PatientHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):
        for index in xrange(51, 99):
            username = '186293573' + str(index)

            password = 'E10ADC3949BA59ABBE56E057F20F883E'

            truenames = ['万丽娜', '李艺彤', '冯薪朵', '鞠婧祎', '李佳恩', '杨韫玉', '邵雪聪', '许杨玉琢']

            truename = choice(truenames)

            nicknames = ['小万', '小李', '小冯', '小鞠', '小李', '小杨', '小邵', '小许']

            nickname = choice(nicknames)

            identiy = 0

            city_id = ObjectId("570e58180e7051acb7ad32df")

            user = User()
            user.username = username
            user.password = password
            user.nickname = nickname
            user.truename = truename
            user.identiy = identiy
            user.city_id = city_id
            user.save()


class CommentHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):

        doctors = User.objects(identiy=1)

        patients = User.objects(identiy=0)

        contents = ['好', '威武', '支持', '有希望了', '依法治国', '今天是个好日子']

        grade = [1, 2, 3, 4, 5]

        date = datetime.today()

        for doctor in doctors:
            for index in xrange(0, 100):
                comment = Comment()
                comment.user_id = choice(patients).id
                comment.doctor_id = doctor.id
                comment.content = choice(contents)
                comment.create_date = date
                comment.grade = choice(grade)
                comment.save()


class RecordHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):

        doctors = User.objects(identiy=1)

        complaints = ['头晕恶心', '消化不良', '恶心呕吐', '发烧发热', '腰酸背痛']

        inspects = ['血常规', 'CT扫描', '尿常规', '心电图', 'X光片']

        diagnosis = ['骨折', '心脏病', '上呼吸道感染', '颈椎病', '腰椎间盘突出']

        treats = ['阿奇霉素注射液', '随访', '阿莫西林胶囊', '牵引', '心脏搭桥手术']

        for doctor in doctors:
            for patient in doctor.patients:
                for index in xrange(0, 50):
                    record = Record()
                    record.complaint = choice(complaints)
                    record.inspect = choice(inspects)
                    record.diagnosis = choice(diagnosis)
                    record.treat = choice(treats)
                    record.doctor_id = doctor.id
                    record.patient_id = patient.id
                    record.create_date = datetime.today()
                    record.save()
