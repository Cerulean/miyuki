# coding=utf-8

from app.base.requestHandler import JsonRequestHandler
from app.base.models.location import City
from app.base.config import *


class CitiesHandler(JsonRequestHandler):
    def get(self, *args, **kwargs):
        cities = City.objects()
        return self.render_json(CODE_SUCCESS, '获取城市列表成功', cities)