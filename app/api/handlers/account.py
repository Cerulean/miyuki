# coding=utf-8

from tornado.httputil import url_concat
from tornado.web import asynchronous
from tornado import httpclient

from mongoengine.errors import ValidationError

from app.base.requestHandler import JsonRequestHandler, FormRequestHandler, logged
from app.base.models.account import *
from app.base.helpers.tools import *

from app.base import config

import os
import json
import urllib
import server


class SmscodeHandler(JsonRequestHandler):
    def get(self):
        username = self.get_argument('username')

        if not re.match(config.TEL_PATTERN, username):
            return self.render_json(config.CODE_FAILURE, "用户名非法")

        smscode = self.redis_cli.get_smscode(username)

        if smscode:
            return self.render_json(config.CODE_FAILURE, "验证码未过期")

        # TODO 请求验证码
        smscode = '0000'
        self.redis_cli.set_smscode(username, smscode)

        if smscode:
            return self.render_json(config.CODE_SUCCESS, "验证码发送成功")
        else:
            return self.render_json(config.CODE_FAILURE, "验证码发送失败")


class RegisterHandler(JsonRequestHandler):
    def post(self, *args, **kwargs):
        username = self.get_argument('username')
        password = self.get_argument('password')
        nickname = self.get_argument('nickname', None)
        smscode = self.get_argument('smscode')

        if User.has_registered(username):
            return self.render_json(config.CODE_FAILURE, "该用户已经注册")

        _smscode = self.redis_cli.get_smscode(username)

        if not _smscode:
            return self.render_json(config.CODE_FAILURE, "验证码不存在或已过期")

        if _smscode != smscode:
            return self.render_json(config.CODE_FAILURE, "短信验证码错误")

        # 短信验证码正确则用户名一定合法
        if not re.match(config.PASSWORD_PATTERN, password):
            return self.render_json(config.CODE_FAILURE, "密码不合法")

        user = User()
        user.username = username
        user.password = Tools.md5(password)
        user.nickname = nickname
        user.save()

        token = self.redis_cli.generate_token_if_need(str(user.id))

        return self.render_json(config.CODE_SUCCESS, "注册成功", {
            "token": token
        })


class LoginHandler(JsonRequestHandler):
    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')

        # 调试可选

        device_token = self.get_argument('device_token', None)

        user = User.get_user_by_username(username)

        if not user:
            return self.render_json(config.CODE_FAILURE, "该用户不存在")

        if user.password != password:
            return self.render_json(config.CODE_FAILURE, "用户名或密码错误")

        user.device_token = device_token
        user.save()

        token = self.redis_cli.generate_token_if_need(str(user.id))

        return self.render_json(config.CODE_SUCCESS, "登录成功", {
            "token": token
        })


class LogoutHandler(JsonRequestHandler):
    @logged
    def post(self, *args, **kwargs):

        user = self.current_user
        user.device_token = None
        user.save()

        return self.render_json(config.CODE_SUCCESS, "登出成功")


class ForgetHandler(JsonRequestHandler):
    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        smscode = self.get_argument('smscode')

        _smscode = self.redis_cli.get_smscode(username)

        if not _smscode:
            return self.render_json(config.CODE_FAILURE, "验证码不存在或已过期")

        if _smscode != smscode:
            return self.render_json(config.CODE_FAILURE, "短信验证码错误")

        if not re.match(config.PASSWORD_PATTERN, password):
            return self.render_json(config.CODE_FAILURE, "密码不合法")

        user = User.get_user_by_username(username)
        user.password = Tools.md5(password)
        user.save()

        return self.render_json(config.CODE_SUCCESS, "密码重设成功")


class InfoHandler(JsonRequestHandler):
    @logged
    def post(self):
        nickname = self.get_argument('nickname', None)
        truename = self.get_argument('truename', None)

        email = self.get_argument('email', None)

        city_id = self.get_argument('city_id', None)

        hobbies = self.get_argument('hobbies', None)

        user = self.current_user

        if nickname:
            if re.match(config.NICKNAME_PATTERN, nickname):
                return self.render_json(config.CODE_FAILURE, "昵称不合法")
            user.nickname = nickname

        if truename:
            if not re.match(config.TRUENAME_PATTERN, truename):
                return self.render_json(config.CODE_FAILURE, "姓名不合法")
            user.truename = truename

        if email:
            user.email = email

        if city_id:
            user.city_id = city_id

        if hobbies:
            if not isinstance(hobbies, list):
                return self.render_json(config.CODE_FAILURE, "爱好必须是字符串数组")
            user.hobbies = hobbies

        try:
            user.save()
        except ValidationError:
            return self.render_json(config.CODE_FAILURE, "邮箱格式非法")

        return self.render_json(config.CODE_SUCCESS, "修改信息成功", user)

    @logged
    def get(self, *args, **kwargs):
        return self.render_json(config.CODE_SUCCESS, "获取用户信息成功", self.current_user, "to_user_info")


class AvatarHandler(FormRequestHandler):
    @logged
    @asynchronous
    def post(self):
        files = self.get_files_argument('avatar')

        avatar_dic = files[0]

        try:
            extension = 'jpg'
            content_type = avatar_dic['content_type'].split('/')[-1]
        except ValueError:
            return self.render_json(config.CODE_FAILURE, "文件没有扩展名")

        if not re.match(config.AVATAR_TYPE, content_type):
            return self.render_json(config.CODE_FAILURE, "仅支持PNG和JPG格式的图片")

        user = self.current_user

        img_dir = os.path.join(server.base_dir, config.IMG_USER_PATH, Tools.md5(user.username))

        if not os.path.exists(img_dir):
            os.mkdir(img_dir)

        imgs = os.listdir(img_dir)

        for img in imgs:
            if img.startswith('avatar'):
                os.remove(os.path.join(img_dir, img))

        img_path = os.path.join(img_dir, 'avatar.' + extension)

        with open(img_path, 'wb') as file:
            file.write(avatar_dic['body'])

        user.avatar_url = img_path.split('actopper')[1]
        user.save()

        self.render_json(config.CODE_SUCCESS, "上传头像成功")
        self.finish()


class PasswordHandler(JsonRequestHandler):
    @logged
    def post(self):
        old = self.get_argument('old')
        new = self.get_argument('new')

        user = self.current_user

        if old != user.password:
            return self.render_json(config.CODE_FAILURE, "旧密码错误")

        if not re.match(config.PASSWORD_PATTERN, new):
            return self.render_json(config.CODE_FAILURE, "密码不合法")

        user.password = Tools.md5(new)
        user.save()

        return self.render_json(config.CODE_SUCCESS, "密码修改成功")


class SsoHandler(JsonRequestHandler):

    @asynchronous
    def post(self, *args, **kwargs):

        sso_type = self.get_argument('sso_type')

        if sso_type == config.SSO_TYPE_WECHAT:

            code = self.get_argument('code')

            parms = dict()
            parms['appid'] = config.WECHAT_APP_ID
            parms['secret'] = config.WECHAT_APP_SECRET
            parms['grant_type'] = 'authorization_code'
            parms['code'] = code

            url = url_concat('https://api.weixin.qq.com/sns/oauth2/access_token', parms)

            http = httpclient.AsyncHTTPClient()
            http.fetch(url, self.on_response_wechat, validate_cert=False)

        elif sso_type == config.SSO_TYPE_WEIBO:

            access_token = self.get_argument('access_token')

            weibo_id = self.get_argument('weibo_id')

            parms = dict()
            parms['access_token'] = access_token

            url = 'https://api.weibo.com/oauth2/get_token_info'

            http = httpclient.HTTPClient()
            response = http.fetch(url, method='POST', body=urllib.urlencode(parms), validate_cert=False)

            self.on_response_weibo(response, weibo_id)
            return self.finish()

        else:
            self.render_json(config.CODE_FAILURE, "SSO类型错误")
            return self.finish()

    def get(self, *args, **kwargs):
        return self.render_json(config.CODE_SUCCESS, "授权成功")

    def on_response_weibo(self, response, weibo_id):

        results = json.loads(response.body)

        uid = str(results['uid'])

        if not uid == weibo_id:
            return self.render_json(config.CODE_FAILURE, "UID不一致")

        user = User.objects(weibo_id=weibo_id).first()

        if not user:
            return self.render_json(config.CODE_SUCCESS, "登录成功, 需要绑定微博", {
                "weibo_id": weibo_id
            })

        token = self.redis_cli.generate_token_if_need(str(user.id))

        self.render_json(config.CODE_SUCCESS, "微博登录成功", {
            "token": token
        })

    def on_response_wechat(self, response):

        results = json.loads(response.body.decode('utf-8'))

        if 'errorcode' in results.keys():
            self.render_json(config.CODE_FAILURE, results['errmsg'])
            return self.finish()

        # openid

        wechat_id = results['openid']

        user = User.objects(wechat_id=wechat_id).first()

        if not user:
            self.render_json(config.CODE_SUCCESS, "登陆成功, 需要绑定微信", {
                "wechat_id": wechat_id
            })
            return self.finish()

        token = self.redis_cli.generate_token_if_need(str(user.id))

        self.render_json(config.CODE_SUCCESS, "微信登录成功", {
            "token": token
        })

        return self.finish()


class BindHandler(JsonRequestHandler):
    def post(self, *args, **kwargs):

        sso_type = self.get_argument('sso_type')

        username = self.get_argument('username')

        password = self.get_argument('password')

        sso_id = self.get_argument('sso_id')

        user = User.get_user_by_username(username)

        if not user:
            return self.render_json(config.CODE_FAILURE, "用户不存在")

        if not user.password == password:
            return self.render_json(config.CODE_FAILURE, "用户名或密码错误")

        if sso_type == config.SSO_TYPE_WECHAT:
            user.wechat_id = sso_id
        elif sso_type == config.SSO_TYPE_WEIBO:
            user.weibo_id = sso_id
        else:
            return self.render_json(config.CODE_FAILURE, "SSO类型错误")

        user.save()

        token = self.redis_cli.generate_token_if_need(str(user.id))

        return self.render_json(config.CODE_SUCCESS, "绑定社交账号成功", {
            "token": token
        })
