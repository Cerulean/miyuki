# coding=utf-8

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.options
import sys
import os.path

from app.base import config, mongo

base_dir = os.path.dirname(__file__)
config.ROOT_PATH = os.path.dirname(__file__)


def get_instance():

    # 优先加载数据库, 因为内部模块均依赖于它
    mongo.connect_mongo()

    # 自动加载目标模块
    package_name = (sys.argv[2] if len(sys.argv) > 2 else "admin")
    m = __import__("app." + package_name + ".routes", None, locals(), ["get_routes"])
    f = m.get_routes

    # 解析递归形式的路由表
    def parse_recursive_route(recursive_routes, prefix=""):
        def f(recursive_input, prefix, route_list_output):
            for item in recursive_input:
                if len(item[0]) == 0:
                    route_list_output.append((prefix, item[1]))
                    continue

                route = prefix + "/" + (item[0] if item[0][0] != "/" else item[0][1:])
                if isinstance(item[1], list):
                    f(item[1], route, route_list_output)
                else:
                    route_list_output.append((route, item[1]))

        routes = []
        f(recursive_routes, prefix, routes)
        return routes

    package_name_split = package_name.split(".")
    # 为 API 提供版本控制
    if package_name_split[0].lower() == "api":
        package_name_split.insert(1, "v" + config.API_VERSION)
    package_name_split[0] = ""

    handlers = parse_recursive_route(f(), "/".join(package_name_split))

    return tornado.web.Application(
        debug=True,
        compiled_template_cache=False,
        handlers=handlers,
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        template_path=os.path.join(os.path.dirname(__file__), "templates/" + package_name),
    )

if __name__ == "__main__":

    # 这行用来开启Log看性能, 上线后注释
    tornado.options.parse_command_line()

    port = sys.argv[1] if len(sys.argv) > 1 else 7777

    app = get_instance()
    server = tornado.httpserver.HTTPServer(app)
    server.listen(port)

    # 用来提醒Debug模式reload结束
    print("server on")
    tornado.ioloop.IOLoop.current().start()
